---
date: 2018-12-08T15:05:36+10:00
draft: false
image: '/refs/ref-delphi.jpg'
businessurl: ''
---

* Construction des bureaux de Méthode Lab et extension des vestiaires
* Revêtement en résine époxydique sur le sol et travaux d’aménagement (locker rooms, storage) à l’usine
* Construction d’un hangar de stockage et réalisation de deux quais
* Travaux de dallage des zones A, C et D
