---
date: 2018-12-08T15:05:36+10:00
draft: false
image: '/refs/ref-marsa.png'
businessurl: ''
---

* ￼Réparation des socles des bacs de la station de pompage de MARSA MAROC MOHAMMEDIA
* Fourniture, pose de deux bollards et ragréage du couronnement des quais au port de TANGER
* Etanchéité de la passerelle et de la plateforme de l’appontement pétrolier au port de MOHAMMEDIA
* Fourniture et pose de bollards au port de NADOR
