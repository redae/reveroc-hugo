---
date: 2018-12-08T15:05:36+10:00
draft: false
image: '/refs/ref-samir.jpg'
businessurl: ''
---

* Etanchéité du bâtiment administratif de la SAMIR MOHAMMEDIA  et des bureaux de BLENDING
* Etancheite & pose de tuiles aux centres d’estivage d’IFRANE, d’IMMOUZER et de MOULAY BOUSSELHAM
* Revêtement en résine époxydique des bassins d’inspection de la zone 2
* Revêtement antiacide de la nouvelle station de traitement des eaux résiduaires raffinerie SAMIR SIDI KACEM
* Protection anti-feu GPL SAMIR MOHAMEDIA
