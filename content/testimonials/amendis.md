---
date: 2018-12-08T15:05:36+10:00
draft: false
image: '/refs/ref-amendis.jpg'
businessurl: ''
---

* Réhabilitation de 3 réservoirs à Tanger
* Étanchéité des toits des postes de transformation et de la station de pompage de TANJAH BALIA
