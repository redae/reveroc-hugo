---
date: 2018-12-08T15:05:36+10:00
draft: false
image: '/refs/ref-cellulose.jpg'
businessurl: ''
---

* Remise en état de la structure des poteaux et poutres des ponts roulants des deux passages endommagés de l’atelier presse pâte
* Stratification du fond du bac HDB2 et de sa virole de l’atelier presse pâte
* Réalisation d’un dallage industriel au local de stockage pâte
* Revêtement en résine époxydique de l’atelier de déminéralisation
* Cuvelage des parois et radiers du bassin d’eau adoucie
* Etanchéité des terrasses, des villas et l’infirmerie de l’usine
