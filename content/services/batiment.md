---
title: 'Batiment'
date: 2018-11-18T12:33:46+10:00
image: '/services/batiment.jpg'
draft: false
featured: true
weight: 1
---

* Revêtement de sols industriels, sols décoratifs
* Collage pour l’assemblage d’éléments de béton préfabriqués
* Cuvelage, sous-sol, cave, bassin, etc.
* Ragréage, renforcement, étanchéité
