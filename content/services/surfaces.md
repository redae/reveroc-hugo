---
title: "Protection des surfaces"
date: 2018-11-28T15:14:54+10:00
image: "/services/protectionsurfaces.jpg"
featured: true
draft: false
---

* Revêtement des réservoirs d’eau et de pétrole
* Traitement superficiel du béton
* Protection anticorrosive des structures en acier de tous genres
* Restructuration du béton dégradé
* Ignifugeage, guinitage
