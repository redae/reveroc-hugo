---
date: 2018-11-19T10:47:58+10:00
draft: false
weight: 2
---

La société REVEROC – MAROC a été créée en Suisse en 1986 et en 1992 au Maroc. Elle s’est imposée dans le domaine du bâtiment ainsi que le génie civil, spécialisée en particulier dans les travaux spéciaux : isolation thermique, phonique, protection passive, injection, consolidation, renforcement de structures en béton armé, étanchéité, traitement de joints, cuvelage : cave, piscine, galeries, collecteurs, sous-sol, réservoirs, revêtements spéciaux de sol industriels, antiacides, anti-usures.

REVEROC-MAROC est aujourd’hui un partenaire de premier plan des grands donneurs d’ordres ; tout aussi en matière de réhabilitation tout corps d’état, réparation, confortement, renforcement et étanchéité d’ouvrage en béton armé ou précontraint, protection contre l’incendie, ignifugeage, calorifugeage.

La société REVEROC – MAROC est agréée par divers organismes nationaux et internationaux.
